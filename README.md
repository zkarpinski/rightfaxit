RightFaxIt
==========

**(Deprecated Oct 2014)** See T-Driver

Lightweight tool to automatically fax documents out of user defined folders using RightFax.